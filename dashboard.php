<?php
    error_reporting(0);
    // Session start here
    session_start();
    if (!isset($_SESSION['email'])){
        header("location: login.php");
    }
    // Session start here
    //Configuration files
    include "inc/config.php";
    //Configuration files
?>
<!DOCTYPE html>
<html lang="EN">
<head>
    <title>Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="css/styless.css">
</head>
<body>
<div class="container">
    <ul>
        <li><a href="website/">Dashboard</a></li>
        <li><a href="topmenu.php">Top menu</a></li>
        <li><a href="bottom_menu.php">Bottom menu</a></li>
        <li> <a href="#">Conetent</a></li>
        <li><a href="#">Site configuration</a></li>
        <li><a href="#">Multidimensional menu</a></li>
        <span><a href="logout.php">Logout</a></span>
        <span><?php echo $_SESSION['username']; ?></span>
    </ul>
</div>
</body>
</html>