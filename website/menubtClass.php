<?php
namespace menuBEngine;

class menubottomClass{

    const tbl_name = 'menubottom'; //db tbl name
    public $db_conn;  // db connection
    public $arrResult;
    private $tbl_name;

    /**
     * auto calling when function goes start
     */
    public function __construct(){
        global $conn;
        $this->db_conn = $conn;


    }
    public function __destruct()
    {
        $this->db_conn->close();
    }

    /**
     *   additing menu
     */
    public function insertmenub(){
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $arDatab = []; // here array: if data is success
            $arErrorb = []; // here array: if data is error
            foreach ($_POST as $key => $value) { //
                $value = $this->filter_input($value); // here calling function filter input for sanitize post req
                if ($key == 'name') {  // here call post named: name
                    if (preg_match('/^\pL+$/u', $value)) { // only latters input
                        $arDatab[$key] = $value;  // if only latters: success
                    }
                    else {
                        $arErrorb[] = 'Menu name is required'; // if not latters: error
                    }
                }

                elseif ($key == 'descriptions'){ // here call post named: descriptions
                    if (preg_match('/^[a-z0-9 .\-]+$/i', $value)){ // only latters input
                        $arDatab[$key] = $value; // if only latters: success
                    }else{
                        $arErrorb [] = 'Description is required'; /// if not latters: error
                    }
                }

            }
            if (count($arErrorb) > 0) {  // here is logic for count errors
                $html = '<div>';
                foreach ($arErrorb as $error) {
                    $html .= '<div>' . $error . '</div>';

                }
                $html .= '</div>';
                $this->arrResult = array(
                    'status' => 'error',
                    'errors' => $html
                ); // here end logic for count errors
            } else {  // here is logic for insert data in db if errors no
                $sql = "INSERT INTO " . self::tbl_name . " (
                                                            menub,menuinfob)
                                                          VALUES ('{$arDatab['name']}',
                                                                  '{$arDatab['descriptions']}'
                                                                            )";// here end logic for insert data in db if errors no
                if ($this->db_conn->query($sql) === TRUE) {
                    $arSuccess [] = 'Menu created successfully'; // here: if this true create menu
                }
                if (count($arSuccess) > 0) { // here count if not errors message success
                    $html = '<div>';
                    foreach ($arSuccess as $data) {
                        $html .= '<div>' . $data . '</div>';
                    }
                    $html .= '</div>';
                    $this->arrResult = array(
                        'status' => 'success',
                        'ok' => $html
                    );// here and count if not errors message succes
                } else {
                    echo "Error: " . $sql . "<br>" . $this->db_conn->error;
                }
            }
        }
    }
    public function showMenub(){
        $result = $this->db_conn->query("SELECT id,menub FROM " . self::tbl_name);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                echo '<a href="?bottom_id='.$row["id"].'">'.$row["menub"].'</a>';

            }
        }


    }
    public function showDescriptionb(){
        if(isset($_GET['bottom_id'])){
            $id = $_GET['bottom_id'];
            $result = $this->db_conn->query("SELECT * FROM " . self::tbl_name ." WHERE id=".$id);
            if ($result->num_rows > 0){
                $row = $result->fetch_assoc();
                echo $row["menuinfob"];
            }
        }
    }


    /**
     * @param $data
     * @return string
     */
    function filter_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    /**
     * @param $arr
     */
    public function pre($arr)
    {
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
    }

}