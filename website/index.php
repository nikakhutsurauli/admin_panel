<?php
    include "../inc/config.php";

    require "menuClass.php";
    use menuEngine\menutopClass;

    $themenu = new menutopClass();

    require "menubtClass.php";
    use menuBEngine\menubottomClass;

    $thebottom = new menubottomClass();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title></title>
    <meta charset="UTF-8">
    <meta name="description" content="Adminpanel">
    <meta name="keywords" content="Web page with Admin">
    <meta name="author" content="nika">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/styless.css" rel="stylesheet" />
</head>
<body>
<div class="container">
    <header>
        <nav>
            <ul>
                <li><?php  $themenu->showMenu(); ?></li>
            </ul>
        </nav>
    </header>

    <div class="sections">
        <p><?php $themenu->showDescription(); ?></p>
    </div><br>

    <div class="footer">
        <?php $thebottom->showMenub(); ?>
        <p><?php $thebottom->showDescriptionb(); ?></p>
    </div>
</div>

</body>
</html>