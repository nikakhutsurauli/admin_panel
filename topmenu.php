<?php
    error_reporting(0);
    // Session start here
    session_start();
    if (!isset($_SESSION['email'])){
        header("location: login.php");
    }
    // Session start here

    //Configuration files
    include "inc/config.php";
    //Configuration files

    include "website/menuClass.php";
    use menuEngine\menutopClass;
    $themenu = new menutopClass();
    $themenu->insertmenu();
?>
<!DOCTYPE html>
<html lang="EN">
<head>
    <title>Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="css/styless.css">
</head>
<body>
<div class="container">
    <ul>
        <li><a href="website/">Dashboard</a></li>
        <li><a href="topmenu.php">Top menu</a></li>
        <li><a href="bottom_menu.php">Bottom menu</a></li>
        <li> <a href="#">Conetent</a></li>
        <li><a href="#">Site configuration</a></li>
        <li><a href="#">Multidimensional menu</a></li>

        <span><a href="logout.php">Logout</a></span>
        <span><?php echo $_SESSION['username']; ?></span>
    </ul>
    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
      <p>Add Menu Top</p><br>
        <input style="width: 15%;" type="text" name="name" placeholder="name"><br>
        <textarea name="descriptions" rows="5" cols="29"></textarea><br>
      <input style="width: 17%; background-color: #8DC26F;" type="submit" value="Save">
    </form>
    <?php
        if ($themenu->arrResult['status'] == 'error'){
            echo '<p style="color: white;">'.$themenu->arrResult['errors'].'</p>';
        }
        if ($themenu->arrResult['status'] == 'success'){
            echo '<p style="color: white;">'.$themenu->arrResult['ok'].'</p>';
        }
    ?>
</div>
</body>
</html>