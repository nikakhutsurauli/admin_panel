<?php
error_reporting(0);
// Session start here
session_start();
if (isset($_SESSION['email'])){
    header("location: login.php");
}
// Session start here
//Configuration files
include "inc/config.php";
include "engine/engineClass.php";
//Configuration files

//Call the Class
use Engine\classEngine;
// Call the class exem
$userR = new classEngine();
$userR->registerUser();
$userR->email_exists('xucurauli.nika@mail.ru');
$userR->users_exists('Nikusha156');
?>
<!DOCTYPE html>
<html lang="EN">
<head>
    <title>Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Custom Theme files -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- //Custom Theme files -->
    <!-- web font -->
    <link href="//fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i" rel="stylesheet">
    <!-- //web font -->
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
	<!-- main -->
	<div class="main-w3layouts wrapper">
		<h1>SIGN UP</h1>

		<div class="main-agileinfo">
			<div class="agileits-top">
            <p>
	        	<!-- Call Errors or success message-->
	            <?php
	            if ($userR->arrResult['status'] == 'error') {
	                echo $userR->arrResult['errors'];
	            }
	            elseif($userR->arrResult['status'] == 'success')
	            {
	                echo $userR->arrResult['ok'];
	            }
	            ?>
	            <!-- Call Errors or success message-->
            </p>
            <!--Here is registration form-->
			<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>" method="post">
				<input class="text-color-white" type="text" name="name" id="name"  placeholder="Name">
                <input style="margin-top: 5px;" class="text-color-white" id="surname" type="text" name="surname"  placeholder="Surname">
                <input style="margin-top: 5px;" class="text-color-white" id="username" type="text" name="username"  placeholder="Username">
				<input style="margin-top: 5px;" class="text-color-white" id="email" type="text" name="email" placeholder="Email">
				<input style="margin-top: 5px;" class="text-color-white" id="password" type="password" name="password" placeholder="Password">
                <div class="wthree-text">
					<div class="clear"> </div>
				</div>
                <input id="submit" type="submit" id="btnShowMsg" onClick="showMessage()" value="SIGNUP">
            </form>
            <!--Here is registration form-->
	            <!--after register go to login-->
				<p><a href="login.php"> Login Now!</a></p>
	            <!--after register go to login--> 
			</div>
		</div>
       <!--- the bubbles in action --->
		<ul class="colorlib-bubbles">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	   <!--- the bubbles in action --->
	</div>
</body>
</html>