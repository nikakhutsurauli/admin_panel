<?php
namespace Engine;

class classEngine
{
     const tbl_name = 'register_user';
     public $arrResult;
     public $db_conn;
     private $tbl_name;

     public function __construct(){
        global $conn;
        $this->db_conn = $conn;
     }

    /**
     * @param $data
     * @return string
     */
    function filter_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    public function users_exists($username){
        $result = $this->db_conn->query("SELECT username FROM " . self::tbl_name . " WHERE username='" . $username . "'");
        $arrUser = $result->fetch_assoc();
        if ($arrUser['username']) {
           return true;
        } else {
            return false;
        }
    }


    /**
     * user regitration
     */
    public function registerUser()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $arData = [];
            $arError = [];
            foreach ($_POST as $key => $value) {
                
                $value = $this->filter_input($value);
                if ($key == 'name') {
                    if (preg_match('/^\pL+$/u', $value)) {
                        $arData[$key] = $value;
                    } else {
                        $arError[] = 'Name is required';
                    }
                }if ($key == 'surname') {
                    if (preg_match('/^\pL+$/u', $value)) {
                        $arData[$key] = $value;
                    } else {
                        $arError[] = 'Surname is required';
                    }
                }if ($key == 'username') {
                    if ($this->users_exists($value)) {
                          $arError [] = 'Username exists';
                      }
                    if (preg_match('/^[a-z\d_]{2,20}$/i', $value)) {
                        $arData[$key] = $value;
                    } else{
                        $arError [] = 'Username is required';
                    }
                }if ($key == 'email') {
                    if(empty($this->validateEmail($value))){
                     $arError[] = 'Email is required';
              
                    }
                    elseif ($this->validateEmail($value)){
                        if ($this->email_exists($value)){
                            $arError [] = 'Email already exists';
                        }
                    }
                    elseif ($this->validateEmail($value)) {
                        $arData[$key] = $value;
                    }
                    
                }elseif ($key == 'password') {
                    if (preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,12}$/', $value)) {
                        $arData[$key] = md5($value);
                    } else {
                        $arError[] = 'The password must contain at least 8 characters,
                                     it must be  accompanied  by numbers';
                    }
                }
            }
            if (count($arError) > 0) {
                $html = '<div>';
                foreach ($arError as $error) {
                    $html .= $error.'<br>';


                }
                $html .= '</div>';
                $this->arrResult = array(
                    'status' => 'error',
                    'errors' => $html
                );


            } else {
                $sql = "INSERT INTO " . self::tbl_name . " (
                                                         name, 
                                                         surname, 
                                                         username, 
                                                         email,
                                                         password)
                                                          VALUES('{$arData['name']}', 
                                                                '{$arData['surname']}', 
                                                                '{$arData['username']}', 
                                                                '{$arData['email']}',
                                                                '{$arData['password']}'
                                                                            )";
                if ($this->db_conn->query($sql) === TRUE) {
                    $arSuccess [] = 'You are successfully registered';
                }

           if (count($arSuccess) > 0) {
                    $html = '<div>';
                    foreach ($arSuccess as $data) {
                        $html .= $data;
                    }
                    $html .= '</div>';
                    $this->arrResult = array(
                        'status' => 'success',
                        'ok' => $html
                   );
              } else {
                 echo "Error: " . $sql . "<br>" . $this->db_conn->error;
               }
            }
        }
    }

    /**
     * @param $email
     * @return bool
     */
    public function email_exists($email)
    {
        $result = $this->db_conn->query("SELECT email FROM " . self::tbl_name . " WHERE email='" . $email . "'");
        $arrUser = $result->fetch_assoc();
        if ($arrUser['email']) {
           return true;
        } else {
            return false;
        }
    }
     

    /**
     * @param $email
     * @return bool
     */
    public function validateEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        } else {
            return true;
        }
    }
    /**
    * function login users
    *
    */
    public function loginUser()
    { 
       if ($_SERVER['REQUEST_METHOD'] == 'POST') {
           $arError = [];
              $email = $this->filter_input($_POST['email']);
              $username = $this->filter_input($_POST['username']);
              $password = md5($_POST['password']);
               $query = $this->db_conn->query("SELECT username,email,password FROM " . self::tbl_name . " WHERE email='" . $email . "' AND password='" . $password . "'");
              
                $numrows = mysqli_num_rows($query);
                if ($numrows != 0) {
                    $row = mysqli_fetch_assoc($query);
                    $_SESSION['email'] = $row['email'];
                    $_SESSION['username'] = $row['username'];
                    /* Перенаправление браузера */
                    header("Location: dashboard.php");
                }
                if (empty($email)) {
                    $arError [] = 'Email is required ';
                }
                
                elseif (empty($_POST['password'])) {
                    $arError [] = 'Password is required ';
                }
                
                elseif (!$this->validateEmail($email)) {
                    $arError [] = 'Incorrect email format';
                }
                elseif(isset($email) || $password != $row['password']){
                    $arError [] = 'Password is wrong';
                }
            
         }
             if (count($arError > 0)) {
                    $html = '<div>';
                foreach ($arError as $errors) {
                    $html .= $errors;
                }
                $html .= '</div>';
                $this->arrResult = array(
                    'status' => 'error',
                    'errors' => $html
                );
          }
    }

}